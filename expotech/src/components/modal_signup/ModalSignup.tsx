import React from "react";
import Input from "../input/Input";
import Button from "../button/Button";
import Dropdown from "../dropdown/Dropdown";

import "./ModalSignup.css";

type PropsType = {
	onSignupCallback: (event: any) => void;
	onLoginCallback: () => void;
}

const DROPDOWN_OPTIONS = [
	{ label: "Comparecer a eventos", value: "EXHIBITOR" },
	{ label: "Organizar eventos", value: "ORGANIZER" }
]

const ModalSignup = (props: PropsType) => (
	<form className="modalSignup" onSubmit={props.onSignupCallback}>
		<Input customClass="u-bottom-10" label="Nome" name="name" id="name" type="text" />
		<Input customClass="u-bottom-10" label="E-mail" name="email" id="email" type="email" />
		<Input customClass="u-bottom-10" label="Senha" name="password" id="password" type="password" />
		<Input customClass="u-bottom-10" label="Confirmar Senha" name="confirmPassword" id="confirmPassword" type="password" />
		<Dropdown customClass="u-bottom-30" label="Eu quero..." name="userType" id="userType" options={DROPDOWN_OPTIONS} />
		<Button label="Cadastrar" buttonType="primary" customClass="u-bottom-10" type="submit" id="signup-button" />
		<Button label="Fazer Login" buttonType="secondary" type="button" id="signup-button" onClick={props.onLoginCallback} />
	</form>
)

export default ModalSignup;