import React, { DetailedHTMLProps, InputHTMLAttributes } from "react";

import { IMAGE_PLACEHOLDER } from "../../utils/ts/constants";

import "./UploadInput.css";

type OwnPropsType = {
	label: string;
	customClass?: string;
}

type PropsType = OwnPropsType & DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

class UploadInput extends React.Component<PropsType> {
	state = {
		image: IMAGE_PLACEHOLDER
	}

	imageUploaded = (e: any) => {
		const file = e.target.files[0];
		const reader = new FileReader();

		reader.onloadend = () => {
			this.setState({ image: reader.result });
		};

		reader.readAsDataURL(file);
	}

	render() {
		return (
			<div className={`uploadInput-field ${this.props.customClass ? this.props.customClass : ""}`}>
				<div className="uploadInput-imgContainer">
					<img src={this.state.image} alt={this.props.label} />
				</div>
				<label htmlFor="uploadInput-input">{this.props.label}</label>
				<input id="uploadInput-input" type="file" onChange={this.imageUploaded} />
				<input type="hidden" {...this.props} value={this.state.image} />
			</div>
		);
	}
}

export default UploadInput;