import React from "react";
import Input from "../input/Input";
import Button from "../button/Button";
import UploadInput from "../upload_input/UploadInput";

import "./ModalCreateEvent.css";
import Dropdown from "../dropdown/Dropdown";

type PropsType = {
	onCreateCallback: (event: any) => any;
}

const options = [
	{ label: "Educação", value: "PROGRAMMING_TEACHER", active: false },
	{ label: "Front-End", value: "FRONTEND", active: false },
	{ label: "Back-End", value: "BACKEND", active: false },
	{ label: "Dev-Ops", value: "DEVOPS", active: false }
]

const ModalCreateEvent = (props: PropsType) => (
	<form className="modalCreateEvent" onSubmit={props.onCreateCallback}>
		<div className="u-inline-items u-bottom-10">
			<UploadInput customClass="modalCreateEvent-uloadInput-field u-size-2-3" label="Adicionar uma foto" name="photo" id="photo" />
			<div className="u-column-items u-size-1-3">
				<Input customClass="u-bottom-10" label="Título" name="title" id="title" type="text" />
				<Dropdown customClass="u-bottom-10" name="type" id="type" label="Tipo" options={options} />
				<Input customClass="u-bottom-10" label="Data Início" name="startDate" id="startDate" type="text" />
				<Input label="Data Fim" name="endDate" id="endDate" type="text" />
			</div>
		</div>
		<Input customClass="u-bottom-10" label="Descrição" name="description" id="description" type="text" />
		<div className="u-inline-items u-bottom-30">
			<Input label="Altura" name="height" id="height" type="text" />
			<Input label="Largura" name="width" id="width" type="text" />
			<Input label="Comprimento" name="_length" id="_length" type="text" />
		</div>
		<Button label="Proximo" buttonType="primary" customClass="u-bottom-10" type="submit" id="create-button" />
	</form>
)

export default ModalCreateEvent;