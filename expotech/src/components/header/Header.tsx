import React from "react";

import { connect } from "react-redux";
import { signIn, clearUser, signUp, updateProfile } from "../../store/user/actions";
import { UserType } from "../../types/User";
import { UserEnumType, UserEnum } from "../../types/enums/UserTypeEnum";

import Modal from "../modal/Modal";
import ModalLogin from "../modal_login/ModalLogin";
import ModalSignup from "../modal_signup/ModalSignup";
import ModalProfile from "../modal_profile/ModalProfile";
import UserWidget from "../user_widget/UserWidget";
import ModalOrganizersEventsContainer from "../../containers/modal_organizers_events_container/ModalOrganizersEventsContainer";

import "./Header.css";

type PropsType = {
	user: UserType;
	userType: UserEnumType;
	signIn: (email: string, password: string) => Promise<any>;
	signUp: (user: UserType, type: UserEnumType) => Promise<any>;
	updateProfile: (user: UserType, type: UserEnumType) => Promise<any>;
	signOut: () => void;
}

type StateType = {
	isUserWidgetMenuOpen: boolean;
}

class Header extends React.Component<PropsType, StateType>  {
	state = {
		isUserWidgetMenuOpen: false
	}

	openLoginModal = () => {
		Modal.open({ title: "Login", element: <ModalLogin onLoginCallback={this.authHandler} onSignupCallback={this.openSignupModal} /> })
	}

	openSignupModal = () => {
		Modal.open({ title: "Cadastro", element: <ModalSignup onLoginCallback={this.openLoginModal} onSignupCallback={this.signUpHandler} /> })
	}

	openProfileModal = () => {
		Modal.open({ title: "Perfil", element: <ModalProfile userType={this.props.userType} user={this.props.user} onUpdateCallback={this.updateProfileHandler} /> })
	}

	toggleUserWidgetMenu = () => {
		this.setState(prevState => ({ isUserWidgetMenuOpen: !prevState.isUserWidgetMenuOpen }));
	}

	authHandler = async (event: any) => {
		event.preventDefault();
		const email = event.target.elements.email.value;
		const password = event.target.elements.password.value;

		const isLoginSuccessful = await this.props.signIn(email, password);

		if (isLoginSuccessful) {
			Modal.close();
		}
	}

	signUpHandler = async (event: any) => {
		event.preventDefault();
		const name = event.target.elements.name.value;
		const email = event.target.elements.email.value;
		const password = event.target.elements.password.value;
		const confirmPassword = event.target.elements.confirmPassword.value;
		const userType = event.target.elements.userType.value;

		if (password !== confirmPassword) {
			alert("As senhas não batem.");
			return;
		}

		const user: UserType = { name, email, password }

		await this.props.signUp(user, userType).then(Modal.close);
		await this.props.signIn(email, password);
	}

	updateProfileHandler = async (event: any) => {
		event.preventDefault();

		const name = event.target.elements.name.value;
		const email = event.target.elements.email.value;
		const site = event.target.elements.site ? event.target.elements.site.value : null;
		const password = event.target.elements.password.value;
		const confirmPassword = event.target.elements.confirmPassword.value;
		const userType = event.target.elements.userType.value;

		if (password !== confirmPassword) {
			alert("As senhas não batem.");
			return;
		}

		const user: UserType = { ...this.props.user, name, email, password }
		if (site) {
			user.site = site;
		}

		await this.props.updateProfile(user, userType);

		if (updateProfile) {
			await this.props.signIn(email, password);

			Modal.close();
			return;
		}

		alert("Ocorreu um erro ao atualizar o perfil.");
	}

	signOutHandler = () => {
		this.toggleUserWidgetMenu();
		this.props.signOut();
	}

	openMyEvents = () => {
		Modal.open({ title: "Eventos", element: <ModalOrganizersEventsContainer user={this.props.user} /> })
	}

	render() {
		return (
			<div className="header">
				<span className="header-logo" />
				<UserWidget user={this.props.user} loginCallback={this.openLoginModal} onClick={this.toggleUserWidgetMenu} />
				{this.state.isUserWidgetMenuOpen ? (
					<div className="header-menu">
						<span className="header-menu-item" onClick={this.openProfileModal}>Perfil</span>
						{this.props.userType === UserEnum.ORGANIZER ? <span className="header-menu-item" onClick={this.openMyEvents}>Meus Eventos</span> : null}
						<span className="header-menu-item" onClick={this.signOutHandler}>Sair</span>
					</div>
				) : null}
			</div>
		)
	}
}

const mapStateToProps = (state: any) => ({
	user: state.user.data,
	userType: state.user.userType
})

const mapDispatchToProps = (dispatch: Function) => ({
	signIn: (email: string, password: string) => dispatch(signIn(email, password)),
	signUp: (user: UserType, type: UserEnumType) => dispatch(signUp(user, type)),
	updateProfile: (user: UserType, type: UserEnumType) => dispatch(updateProfile(user, type)),
	signOut: () => dispatch(clearUser())
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);