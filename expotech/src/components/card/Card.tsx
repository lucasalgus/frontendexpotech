import React from "react";

import { IMAGE_PLACEHOLDER } from "../../utils/ts/constants";

import "./Card.css"

type PropsType = {
	title: string;
	type: string;
	photo: string;
	organizer: any;
	startDate: string;
	endDate: string;
	onClick: () => void;
}

const Card = (props: PropsType) => (
	<div className="card" onClick={props.onClick}>
		<div className="card-imageContainer">
			<img src={props.photo ? props.photo : IMAGE_PLACEHOLDER} alt={props.title} className="card-image" />
		</div>
		<div className="card-body">
			<strong className="card-title">{props.title}</strong>
			<div className="card-details">
				<span className="card-author">
					Evento por {props.organizer.name}
				</span>
				<span className="card-date">
					{new Date(props.startDate).toLocaleDateString()}
				</span>
			</div>
		</div>
	</div>
);

export default Card;