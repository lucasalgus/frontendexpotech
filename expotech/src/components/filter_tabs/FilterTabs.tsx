import React from "react";

import { FilterTabType } from "../../types/FilterTabType";

import "./FilterTabs.css";

type PropsType = {
	tabs: FilterTabType[];
	filterOption: string;
	showCreateButton: boolean;
	onFilterTabClickedCallback: (type: string) => any;
	onCreateButtonClickedCallback: () => any;
};

const FilterTabs = (props: PropsType) => (
	<div className="filterTabs">
		<div className="filterTabs-tabs">
			{props.tabs.map((tab, index) => (
				<span key={index} onClick={() => props.onFilterTabClickedCallback(tab.value)} className={`filterTabs-tab ${tab.active ? "is-active" : ""}`}>{tab.label}</span>
			))}
		</div>
		{props.showCreateButton ? <span className="filterTabs-tab -button" onClick={props.onCreateButtonClickedCallback}>Novo Evento</span> : null}
	</div>
);

export default FilterTabs;
