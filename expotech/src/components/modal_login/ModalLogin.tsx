import React, { FormEvent } from "react";
import Input from "../input/Input";
import Button from "../button/Button";

import "./ModalLogin.css";

type PropsType = {
	onLoginCallback: (event: FormEvent) => void;
	onSignupCallback: () => void;
}

const ModalLogin = (props: PropsType) => (
	<form className="modalLogin" onSubmit={props.onLoginCallback}>
		<Input label="E-mail" name="email" customClass="u-bottom-10" id="email" type="email" />
		<Input label="Senha" name="password" customClass="u-bottom-30" id="password" type="password" />
		<Button label="Entrar" buttonType="primary" customClass="u-bottom-10" type="submit" id="login-button" />
		<Button label="Fazer cadastro" buttonType="secondary" type="button" id="signup-button" onClick={props.onSignupCallback} />
	</form>
)

export default ModalLogin;