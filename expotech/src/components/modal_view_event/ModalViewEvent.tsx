import React from "react";

import Button from "../button/Button";

import "./ModalViewEvent.css";
import { IMAGE_PLACEHOLDER } from "../../utils/ts/constants";

type PropsType = {
	event: any;
	showSubscribe: boolean;
	subscribeCallback: () => void;
}

const ModalViewEvent = (props: PropsType) => {
	return (
		<div className="modalViewEvent">
			<div className="u-inline-items u-bottom-30">
				<div className="u-column-items u-size-1-2">
					<div className="modalViewEvent-imageContainer">
						<img className="modalViewEvent-image" src={props.event.photo ? props.event.photo : IMAGE_PLACEHOLDER} alt={props.event.title} />
					</div>
				</div>
				<div className="modalViewEvent-info u-column-items u-size-1-2">
					<h1 className="modalViewEvent-title u-bottom-10">{props.event.title}</h1>
					<div className="modalViewEvent-date">
						De {new Date(props.event.startDate).toLocaleDateString()} até {new Date(props.event.endDate).toLocaleDateString()}
					</div>
				</div>
			</div>
			<div className="u-bottom-30">
				<p>{props.event.description}</p>
			</div>
			{props.showSubscribe ? (
				<Button label="Inscrever" buttonType="primary" customClass="u-bottom-10" type="submit" id="create-button" onClick={props.subscribeCallback} />
			) : null}
		</div>
	)
}

export default ModalViewEvent;