import React from "react";

import "./ModalAddBooths.css";
import Button from "../button/Button";
import Input from "../input/Input";

type PropsType = {
	eventTitle: string;
	eventImage: string;
	startDate: string;
	endDate: string;
	booths: any[];
	chooseBoothCallback: (e: any) => void;
	removeBoothHandler: (booth: any) => void;
	createEventCallback: () => void;
}

const ModalAddBooths = (props: PropsType) => (
	<div className="modalAddBooths">
		<div className="modalAddBooths-header">
			<div className="modalAddBooths-header-imageContainer">
				<img className="modalAddBooths-header-image" src={props.eventImage} />
			</div>
			<div className="modalAddBooths-header-info">
				<h1 className="modalAddBooths-header-title">{props.eventTitle}</h1>
				<div className="modalAddBooths-header-dates">
					<div className="modalAddBooths-header-date">
						{new Date(props.startDate).toLocaleDateString()}
					</div>
					<div className="modalAddBooths-header-date">
						{new Date(props.endDate).toLocaleDateString()}
					</div>
				</div>
			</div>
		</div>
		<div className="modalAddBooths-body">
			{props.booths.map((b, index) => (
				<div key={index} className="modalAddBooths-booth u-bottom-10">
					<div className="modalAddBooths-booth-name">
						Estande #{index + 1}
						{console.log(b)}
					</div>
					<div className="modalAddBooths-booth-info">
						{b.dimension.width}x{b.dimension.height}x{b.dimension.length}
					</div>
					<div className="modalAddBooths-booth-info">
						{b.price}
					</div>
					<div className="modalAddBooths-booth-button">
						<Button label="Remover" buttonType="secondary" onClick={() => props.removeBoothHandler(index)} />
					</div>
				</div>
			))}
			<form className="u-bottom-30" onSubmit={props.chooseBoothCallback}>
				<div className="modalAddBooths-booth">
					<input type="hidden" name="id" height="id" value={props.booths.length} />
					<div className="modalAddBooths-booth-name">
						Estande #{props.booths.length + 1}
					</div>
					<div className="modalAddBooths-booth-input">
						<Input label="Alt." name="height" id="height" type="text" />
					</div>
					<div className="modalAddBooths-booth-input">
						<Input label="Larg." name="width" id="width" type="text" />
					</div>
					<div className="modalAddBooths-booth-input">
						<Input label="Comp." name="_length" id="_length" type="text" />
					</div>
					<div className="modalAddBooths-booth-input">
						<Input label="Preço" name="price" id="price" type="text" />
					</div>
					<div className="modalAddBooths-booth-button">
						<Button type="submit" label="Adicionar" buttonType="primary" />
					</div>
				</div>
			</form>

			<Button label="Cadastrar Evento" buttonType="primary" onClick={props.createEventCallback} />
		</div>
	</div>
);

export default ModalAddBooths;