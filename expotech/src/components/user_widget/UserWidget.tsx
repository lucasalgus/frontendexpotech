import React from "react";

import "./UserWidget.css";

type PropsType = {
	user: any;
	loginCallback: () => void;
	onClick: () => void;
}

const UserWidget = (props: PropsType) => (
	props.user ? (
		<div className="userWidget" onClick={props.onClick}>
			<span className="userWidget-text">Bem vindo, {props.user.name}</span>
			<img className="userWidget-picture" src={props.user.avatar ? props.user.avatar : "http://www.bigleaf.net/wp-content/uploads/2017/10/avatar-placeholder.png"} alt="Avatar" />
		</div>
	) : (
			<button className="userWidget-login" onClick={props.loginCallback}>Entrar</button>
		)
)

export default UserWidget;