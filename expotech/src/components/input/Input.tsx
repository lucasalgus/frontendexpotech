import React, { DetailedHTMLProps, InputHTMLAttributes } from "react";

import "./Input.css";

type OwnPropsType = {
	label: string
	customClass?: string;
}

type PropsType = OwnPropsType & DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

const Input = (props: PropsType) => (
	<div className={`input-field ${props.customClass ? props.customClass : ""}`}>
		<input className="input" placeholder={props.label} {...props} />
		<label className="input-label" htmlFor={props.id}>{props.label}</label>
	</div>
);

export default Input;