import React, { DetailedHTMLProps, InputHTMLAttributes } from "react";

import "./Dropdown.css";

type DropdownOptionType = {
	label: string;
	value: string;
}

type OwnPropsType = {
	label: string;
	options: DropdownOptionType[];
	customClass?: string;
}

type StateType = {
	isMenuOpen: boolean;
	options: DropdownOptionType[];
	selectedOption?: DropdownOptionType;
}

type PropsType = OwnPropsType & DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

class Dropdown extends React.Component<PropsType, StateType> {
	state: StateType = {
		isMenuOpen: false,
		options: this.props.options,
		selectedOption: undefined
	};

	optionSelected = (option?: DropdownOptionType) => {
		this.setState({ selectedOption: option });
		this.toggleMenu();
	}

	toggleMenu = () => {
		this.setState(prevState => ({ isMenuOpen: !prevState.isMenuOpen }));
	}

	render() {
		return (
			<div className={`dropdown-field ${this.state.selectedOption ? "is-selected" : ""} ${this.state.isMenuOpen ? "is-menuOpen" : ""} ${this.props.customClass}`}>
				<label className="dropdown-label">{this.props.label}</label>
				<div onClick={this.toggleMenu} className="dropdown-input">{this.state.selectedOption ? this.state.selectedOption.label : this.props.label}</div>
				{this.state.isMenuOpen ? (
					<div className="dropdown-menu">
						{this.props.options.map(option => (
							<div key={option.value} className="dropdown-menu-option" onClick={() => this.optionSelected(option)}>{option.label}</div>
						))}
					</div>
				) : null}
				<input {...this.props} type="hidden" value={this.state.selectedOption ? this.state.selectedOption.value : ""} />
			</div>
		);
	}
};

export default Dropdown;
