import React from "react";

import "./Backdrop.css";

type BackdropOptionsType = {
	onClick: () => void;
}

class Backdrop extends React.Component {
	state = {
		isOpen: false,
	}

	static backdrop: Backdrop;
	private onClick?: () => void;

	componentDidMount() {
		Backdrop.backdrop = this;
	}

	static open(options: BackdropOptionsType) {
		Backdrop.backdrop.open(options);
	}

	static close() {
		Backdrop.backdrop.close();
	}

	private open = (options: BackdropOptionsType) => {
		this.setState({ isOpen: true });
		this.onClick = options.onClick;
	}

	private close = () => {
		this.setState({ isOpen: false, element: null });
	}

	onClickHandler = () => {
		if (this.onClick) {
			this.onClick();
		}
		this.close();
	}

	render() {
		return (
			<div onClick={this.onClickHandler} className={`backdrop ${this.state.isOpen ? "is-open" : ""}`} />
		)
	}
}

export default Backdrop;