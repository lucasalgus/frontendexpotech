import React from "react";
import Backdrop from "../backdrop/Backdrop";

import "./Modal.css";

type ModalOptionsType = {
	element: JSX.Element;
	title: string;
};

class Modal extends React.Component {
	state = {
		isOpen: false,
		element: null,
		title: ""
	}

	static modal: Modal;

	componentDidMount() {
		Modal.modal = this;
	}

	static open(options: ModalOptionsType) {
		Modal.modal.open(options);
		Backdrop.open({ onClick: this.close });
	}

	static close() {
		Modal.modal.close();
		Backdrop.close();
	}

	private open = (options: ModalOptionsType) => {
		this.setState({ isOpen: true, element: options.element, title: options.title });
	}

	private close = () => {
		this.setState({ isOpen: false, element: null, title: "" });
	}

	render() {
		return (
			<div className={`modal ${this.state.isOpen ? "is-open" : ""}`}>
				<h1 className="modal-title">{this.state.title}</h1>
				{this.state.element}
			</div>
		)
	}
}

export default Modal;