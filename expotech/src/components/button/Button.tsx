import React, { DetailedHTMLProps, ButtonHTMLAttributes } from "react";

import "./Button.css";

type OwnPropsType = {
	label: string;
	buttonType: "primary" | "secondary";
	customClass?: string;
}

type PropsType = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & OwnPropsType;

const Button = (props: PropsType) => (
	<div className="button-field">
		<button className={`button -${props.buttonType} ${props.customClass ? props.customClass : ""}`} {...props}>{props.label}</button>
	</div>
);

export default Button;