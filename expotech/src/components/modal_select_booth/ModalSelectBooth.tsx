import React from "react";
import Button from "../button/Button";

type PropsType = {
	event: any;
	selectedId: number;
	selectBoothCallback: (index: number) => void;
	subscribeCallback: () => void;
}

const ModalSelectBooth = (props: PropsType) => (
	<div className="modalAddBooths">
		<div className="modalAddBooths-header">
			<div className="modalAddBooths-header-imageContainer">
				<img className="modalAddBooths-header-image" src={props.event.photo} />
			</div>
			<div className="modalAddBooths-header-info">
				<h1 className="modalAddBooths-header-title">{props.event.title}</h1>
				<div className="modalAddBooths-header-dates">
					<div className="modalAddBooths-header-date">
						{new Date(props.event.startDate).toLocaleDateString()}
					</div>
					<div className="modalAddBooths-header-date">
						{new Date(props.event.endDate).toLocaleDateString()}
					</div>
				</div>
			</div>
		</div>
		<div className="modalAddBooths-body">
			<div className="u-bottom-40">
				{props.event.booths.map((b: any, index: number) => (
					<div key={index} className="modalAddBooths-booth u-bottom-10">
						<div className="modalAddBooths-booth-name">
							Estande #{index + 1}
						</div>
						<div className="modalAddBooths-booth-info">
							{b.dimension.width}x{b.dimension.height}x{b.dimension.length}
						</div>
						<div className="modalAddBooths-booth-info">
							R$: {b.price}
						</div>
						<div className="modalAddBooths-booth-button">
							<Button
								label={props.selectedId === b.id ? "Escolhido" : b.exhibitor !== undefined ? "Ocupado" : "Escolher"}
								buttonType="secondary" onClick={() => props.selectBoothCallback(index)}
								title={b.exhibitor ? "Este estande já está ocupado" : undefined}
								disabled={b.exhibitor !== undefined}
							/>
						</div>
					</div>
				))}
			</div>

			<Button label="Inscrever" buttonType="primary" onClick={props.subscribeCallback} />
		</div>
	</div>
);

export default ModalSelectBooth;