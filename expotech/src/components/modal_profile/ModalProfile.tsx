import React from "react";
import Input from "../input/Input";
import Button from "../button/Button";

import "./ModalProfile.css";
import { UserEnumType, UserEnum } from "../../types/enums/UserTypeEnum";
import { UserType } from "../../types/User";

type PropsType = {
	userType: UserEnumType;
	user: UserType;
	onUpdateCallback: (event: any) => void;
}

const ModalProfile = (props: PropsType) => (
	<form className="modalProfile" onSubmit={props.onUpdateCallback}>
		<Input customClass="u-bottom-10" label="Nome" name="name" id="name" type="text" defaultValue={props.user.name || ""} />
		<Input customClass="u-bottom-10" label="E-mail" name="email" id="email" type="email" defaultValue={props.user.email || ""} />
		{props.userType === UserEnum.ORGANIZER ? (
			<Input customClass="u-bottom-10" label="Site" name="site" id="site" type="text" defaultValue={props.user.site || ""} />
		) : null}
		<Input customClass="u-bottom-10" label="Senha" name="password" id="password" type="password" />
		<Input customClass="u-bottom-30" label="Confirmar Senha" name="confirmPassword" id="confirmPassword" type="password" />
		<input type="hidden" name="userType" id="userType" value={props.userType} />
		<Button label="Atualizar" buttonType="primary" customClass="u-bottom-10" type="submit" id="update-button" />
	</form>
)

export default ModalProfile;