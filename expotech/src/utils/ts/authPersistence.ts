import { UserType } from "../../types/User";
import { UserEnumType } from "../../types/enums/UserTypeEnum";
import { LoginResponseType } from "../../types/responses/LoginResponseType";

export const saveUserToLocalStorage = (user?: UserType, userType?: UserEnumType) => {
	if (!user || !userType) {
		return;
	}

	localStorage.setItem("user", JSON.stringify(user));
	localStorage.setItem("userType", userType);
};

export const retrieveUserFromLocalStorage = (): LoginResponseType | undefined => {
	const userData = localStorage.getItem("user");
	const userType = localStorage.getItem("userType") as UserEnumType;

	if (userData && userType) {
		return { auth: true, user: (JSON.parse(userData) as UserType), userType };
	}
};

export const clearUserFromLocalStorage = () => {
	localStorage.removeItem("user");
};