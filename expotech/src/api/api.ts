import { LoginResponseType } from "../types/responses/LoginResponseType";
import { UserType } from "../types/User";
import { UserEnumType } from "../types/enums/UserTypeEnum";

const url = "http://localhost:8080/api";

const API = {
	get: async (endpoint: string) => {
		const promise = await fetch(url + endpoint);
		const json = promise.json();
		
		return json;
	},
	post: async (endpoint: string = "", data: any, noJson?: boolean) => {
		const promise = await fetch(url + endpoint, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				...data
			}),
		});
		if (noJson) {
			return true;
		}

		const json = promise.json();
		
		return json;
	},
	put: async (endpoint: string = "", data: any) => {
		const promise = await fetch(url + endpoint, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				...data
			}),
		});

		const json = promise.json();
		
		return json;
	}
}

export const loginWithEmailAndPassword = (email: string, password: string): Promise<LoginResponseType> => {
	return API.post(`/user/login`, { email, password });
}

export const signUpNewUser = (user: UserType, type: UserEnumType): Promise<LoginResponseType> => {
	return API.post(`/${type.toLowerCase()}/add`, user);
}

export const updateUser = (user: UserType, type: UserEnumType): Promise<LoginResponseType> => {
	return API.put(`/${type.toLowerCase()}/update`, user);
}

export const fetchAllEvents = (type: string): Promise<any> => {
	return API.get(`/event/all${type ? `By?type=${type}` : ""}`);
}

export const postEvent = (event: any): Promise<any> => {
	return API.post("/event/add", event);
}

export const addExhibitor = (subscription: any): Promise<any> => {
	return API.post("/event/addExhibitor", subscription, true);
}

export const fetchAllEventsFromOrganizer = (organizerId: number): Promise<any> => {
	return API.get(`/event/allFromOrganizer?id=${organizerId}`);
}
