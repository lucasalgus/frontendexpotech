import React from "react";
import { connect } from "react-redux";

import FilterTabs from "../../components/filter_tabs/FilterTabs";
import Card from "../../components/card/Card";

import Modal from "../../components/modal/Modal";
import { UserEnumType, UserEnum } from "../../types/enums/UserTypeEnum";
import { UserType } from "../../types/User";
import { getAllEvents, createNewEvent } from "../../store/event/actions";
import { FilterTabType } from "../../types/FilterTabType";

import "./Home.css"
import ModalCreateEventContainer from "../modal_create_event_container/ModalCreateEventContainer";
import ModalSubscribeEventContainer from "../modal_subscribe_event_container/ModalSubscribeEventContainer";

type PropsType = {
	events: any[];
	user: UserType
	userType: UserEnumType;
	getAllEvents: (type: string) => any;
}

type StateType = {
	tabs: FilterTabType[];
	filterOption: string;
}

class Home extends React.Component<PropsType, StateType> {
	state = {
		filterOption: "",
		tabs: [
			{ label: "Todos", value: "", active: true },
			{ label: "Ensino de Programação", value: "PROGRAMMING_TEACHER", active: false },
			{ label: "Front-End", value: "FRONTEND", active: false },
			{ label: "Back-End", value: "BACKEND", active: false },
			{ label: "Dev-Ops", value: "DEV_OPS", active: false }
		]
	}

	componentDidMount() {
		this.fetchEvents();
	}

	fetchEvents = () => {
		this.props.getAllEvents(this.state.filterOption);
	}

	openCard = (event: any) => {
		if (this.props.userType !== UserEnum.ORGANIZER) {
			Modal.open({ title: "Evento", element: <ModalSubscribeEventContainer event={event} /> });
		}
	}

	openCreateNewEventModal = () => {
		Modal.open({ title: "Criar Evento", element: <ModalCreateEventContainer /> })
	}

	filterEvents = (type: string) => {
		this.setState(prevState => ({
			tabs: prevState.tabs.map(t => ({
				...t,
				active: t.value === type
			})),
			filterOption: type
		}), this.fetchEvents);
	}

	render() {
		return (
			<div className="home">
				<FilterTabs
					tabs={this.state.tabs}
					filterOption={this.state.filterOption}
					showCreateButton={this.props.userType === UserEnum.ORGANIZER}
					onFilterTabClickedCallback={this.filterEvents}
					onCreateButtonClickedCallback={this.openCreateNewEventModal}
				/>

				<div className="home-cards">
					{(this.props.events || []).map((event: any) => (
						<Card key={event.id} {...event} onClick={() => this.openCard(event)} />
					))}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state: any) => ({
	userType: state.user.userType,
	user: state.user.data,
	events: state.event.events
});

const mapDispatchToProps = (dispatch: any) => ({
	getAllEvents: (type: string) => dispatch(getAllEvents(type))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);