import React, { Component } from "react";
import ModalCreateEvent from "../../components/modal_create_event/ModalCreateEvent";
import ModalAddBooths from "../../components/modal_add_booths/ModalAddBooths";
import Modal from "../../components/modal/Modal";
import { connect } from "react-redux";
import { UserType } from "../../types/User";
import { createNewEvent } from "../../store/event/actions";

type PropsType = {
	user: UserType;
	createNewEvent: (event: any) => any;
}

type StateType = {
	page: number;
	event: any;
}

class ModalCreateEventContainer extends Component<PropsType, StateType> {
	state: StateType = {
		page: 1,
		event: {
			booths: []
		}
	}

	saveEventDataHandler = (e: any) => {
		e.preventDefault();

		const title = e.target.elements.title.value;
		const photo = e.target.elements.photo.value;
		const type = e.target.elements.type.value;
		const description = e.target.elements.description.value;
		const startDate = new Date(e.target.elements.startDate.value).toISOString();
		const endDate = new Date(e.target.elements.endDate.value).toISOString();
		const height = +e.target.elements.height.value;
		const width = +e.target.elements.width.value;
		const length = +e.target.elements._length.value;

		this.setState(prevState => ({
			event: {
				...prevState.event,
				organizerId: this.props.user.id,
				description,
				title,
				type,
				photo,
				startDate,
				endDate,
				dimension: {
					width,
					height,
					length
				}
			}
		}))

		this.setState({ page: 2 });
	}

	chooseBoothHandler = (e: any) => {
		e.preventDefault();

		const id = +e.target.elements.id.value;
		const price = +e.target.elements._length.value;
		const height = +e.target.elements.height.value;
		const width = +e.target.elements.width.value;
		const length = +e.target.elements._length.value;

		const booth = {
			id,
			price,
			dimension: {
				height,
				width,
				length
			}
		}

		this.setState(prevState => ({
			event: {
				...prevState.event,
				booths: [
					...prevState.event.booths,
					booth
				]
			}
		}))
	}

	removeBoothHandler = (index: number) => {
		this.setState(prevState => {
			let booths = prevState.event.booths.splice(index, 1);
			booths = prevState.event.booths.splice(index, 1);

			return {
				event: {
					...prevState.event,
					booths
				}
			}
		})
	}

	createEventHandler = () => {
		debugger;
		this.props.createNewEvent(this.state.event).then(() => {
			Modal.close();
		});
	}

	render() {
		return (
			this.state.page === 1 ? (
				<ModalCreateEvent onCreateCallback={this.saveEventDataHandler} />
			) :
				this.state.page === 2 ? (
					<ModalAddBooths
						booths={this.state.event.booths}
						eventTitle={this.state.event.title}
						eventImage={this.state.event.photo}
						startDate={this.state.event.startDate}
						endDate={this.state.event.endDate}
						chooseBoothCallback={this.chooseBoothHandler}
						createEventCallback={this.createEventHandler}
						removeBoothHandler={this.removeBoothHandler}
					/>
				) :
					<>
						{Modal.close()}
					</>
		)
	}
}

const mapStateToProps = (state: any) => ({
	user: state.user.data
})

const mapDispatchToProps = (dispatch: any) => ({
	createNewEvent: (event: any) => dispatch(createNewEvent(event))
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalCreateEventContainer);