import React, { Component } from "react";
import { fetchAllEventsFromOrganizer } from "../../api/api";
import { UserType } from "../../types/User";

import "./ModalOrganizersEventsContainer.css";
import Modal from "../../components/modal/Modal";
import ModalEventExhibitors from "../modal_event_exhibitors/ModalEventExhibitors";

type PropsType = {
	user: UserType;
}

type StateType = {
	events: any[];
}

class ModalOrganizersEventsContainer extends Component<PropsType, StateType> {
	state: StateType = {
		events: []
	}

	componentDidMount() {
		if (this.props.user.id) {
			fetchAllEventsFromOrganizer(this.props.user.id).then(events => {
				this.setState({
					events
				});
			});
		}
	}

	openExhibitorsModal = (event: any) => {
		Modal.open({ title: "Expositores", element: <ModalEventExhibitors exhibitors={event.exhibitors} /> })
	}

	render() {
		return (
			<div>
				{this.state.events.map(e => (
					<div key={e.key} className="event" onClick={() => this.openExhibitorsModal(e)}>
						<div className="event-imageContainer">
							<img src={e.photo} alt="" className="event-image" />
						</div>
						<strong className="event-title">{e.title}</strong>
					</div>
				))}
			</div>
		);
	}
}

export default ModalOrganizersEventsContainer;