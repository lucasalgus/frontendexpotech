import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";

import Home from "../home/Home";
import Header from "../../components/header/Header";
import Modal from "../../components/modal/Modal";
import Backdrop from "../../components/backdrop/Backdrop";

import "./App.css";
import { store } from "../../store/store";
import authProvider from "../../hoc/authProvider";

class App extends React.Component {
	render() {
		return (
			<div className="app">
				<Provider store={store}>
					<Header />
					<BrowserRouter>
						<Switch>
							<Route exact={true} path="/" component={authProvider(Home, false)} />
						</Switch>
					</BrowserRouter>
					<Modal />
					<Backdrop />
				</Provider>
			</div>
		);
	}
}

export default App;
