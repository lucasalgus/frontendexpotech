import React from "react";

import "./ModalEventExhibitors.css";

type PropsType = {
	exhibitors: any[]
}

const ModalEventExhibitors = (props: PropsType) => {
	return (
		<>
			{props.exhibitors.map(e => (
				<div className="modalEventExhibitors-exhibitor u-bottom-10">
					<div className="modalEventExhibitors-exhibitor-imageContainer">
						<img src={e.avatar ? e.avatar : "http://www.bigleaf.net/wp-content/uploads/2017/10/avatar-placeholder.png"} alt="" className="modalEventExhibitors-exhibitor-image" />
					</div>
					<strong className="modalEventExhibitors-exhibitor-name">{e.name}</strong>
				</div>
			))}
		</>
	);
}

export default ModalEventExhibitors;