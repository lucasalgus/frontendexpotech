import React, { Component } from "react";
import ModalViewEvent from "../../components/modal_view_event/ModalViewEvent";
import ModalSelectBooth from "../../components/modal_select_booth/ModalSelectBooth";
import { connect } from "react-redux";
import { UserType } from "../../types/User";
import { addExhibitor } from "../../api/api";
import Modal from "../../components/modal/Modal";

type PropsType = {
	event: any;
	user?: UserType;
}

type StateType = {
	page: number;
	subscription: any;
}

class ModalSubscribeEventContainer extends Component<PropsType, StateType> {
	state: StateType = {
		page: 1,
		subscription: {
			idEvent: null,
			idExhibitor: null,
			idBooth: null
		}
	}

	componentDidMount() {
		this.setState({
			subscription: {
				idEvent: this.props.event.id,
				idExhibitor: this.props.user ? this.props.user.id : null
			}
		})
	}

	nextPage = () => {
		this.setState({ page: 2 });
	}

	subscribeHandler = () => {
		addExhibitor(this.state.subscription).then(() => {
			Modal.close();
		});
	}

	selectBoothHandler = (index: number) => {
		this.setState(prevState => ({
			...prevState,
			subscription: {
				...prevState.subscription,
				idBooth: index
			}
		}));
	}

	render() {
		return (
			this.state.page === 1 ? (
				<ModalViewEvent showSubscribe={this.props.user !== undefined} event={this.props.event} subscribeCallback={this.nextPage} />
			) :
				this.state.page === 2 ? (
					<ModalSelectBooth event={this.props.event} selectedId={this.state.subscription.idBooth} selectBoothCallback={this.selectBoothHandler} subscribeCallback={this.subscribeHandler} />
				) : null
		);
	}
}

const mapStateToProps = (state: any) => ({
	user: state.user.data
})

export default connect(mapStateToProps)(ModalSubscribeEventContainer);