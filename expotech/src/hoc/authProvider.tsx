import React from "react";
import { connect } from "react-redux";

import { UserType } from "../types/User";
import { retrieveUser } from "../store/user/actions";

type PropsType = {
	user: UserType;
	retrieveUser: () => UserType;
}

const authProvider = (Component: any, isProtected: boolean) => {
	const AuthComponent = (props: PropsType) => {
		if (!props.user) {
			const user = props.retrieveUser();

			if (!user && isProtected) {
				window.location.href = "/";

				return <></>;
			}
		}

		return <Component {...props} />;
	};

	const mapStateToProps = (state: any) => ({
		user: state.user.data
	});

	const mapDispatchToProps = (dispatch: any) => ({
		retrieveUser: () => dispatch(retrieveUser())
	});

	return connect(
		mapStateToProps,
		mapDispatchToProps
	)(AuthComponent);
};

export default authProvider;