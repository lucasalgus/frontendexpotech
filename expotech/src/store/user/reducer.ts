import { GET_USER_BEGIN, GET_USER_SUCCESS, GET_USER_FAILURE, CLEAR_USER } from "./actions";

const initialState: any = {
	status: null,
	data: null,
	error: null,
	userType: null
}

// É executado quando uma action é despachada e salva dados no store
export const user = (state = initialState, action: any) => {
	switch (action.type) {
		case GET_USER_BEGIN:
			return {
				...state,
				status: "loading"
			}
		case GET_USER_SUCCESS:
			return {
				...state,
				status: "success",
				data: action.data,
				userType: action.userType
			}
		case GET_USER_FAILURE:
			return {
				...state,
				status: "error",
				error: action.error
			}
		case CLEAR_USER:
			return initialState
		default:
			return state;
	}
}