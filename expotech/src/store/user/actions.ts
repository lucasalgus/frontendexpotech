import { loginWithEmailAndPassword, signUpNewUser, updateUser } from "../../api/api";
import { UserType } from "../../types/User";
import { saveUserToLocalStorage, retrieveUserFromLocalStorage, clearUserFromLocalStorage } from "../../utils/ts/authPersistence";
import { UserEnumType } from "../../types/enums/UserTypeEnum";

export const GET_USER_BEGIN = "GET_USER_BEGIN";
export const GET_USER_SUCCESS = "GET_USER_SUCCESS";
export const GET_USER_FAILURE = "GET_USER_FAILURE";
export const CLEAR_USER = "CLEAR_USER";

const getUserBegin = () => ({
	type: GET_USER_BEGIN
});

const getUserSuccess = (data: UserType, userType: UserEnumType) => ({
	type: GET_USER_SUCCESS,
	data,
	userType
});

const getUserFailure = (error: any) => ({
	type: GET_USER_FAILURE,
	error
});

export const clearUser = () => {
	clearUserFromLocalStorage();

	return {
		type: CLEAR_USER
	}
};

export const retrieveUser = () => {
	return (dispatch: any) => {
		const userData = retrieveUserFromLocalStorage();
		
		if (userData) {
			dispatch(getUserSuccess(userData.user, userData.userType));
		}
	}
}

// Action que faz o request
export const signIn = (email: string, password: string) => {
	return (dispatch: any) => {
		// Despacha a begin action
		dispatch(getUserBegin());

		return loginWithEmailAndPassword(email, password)
			.then(response => {
				const { user, userType } = response;
				// Despacha a action de sucesso e salva no localStorage
				dispatch(getUserSuccess(user, userType));
				saveUserToLocalStorage(user, userType);
				return true;
			})
			.catch(error => {
				// Despacha a action de erro
				dispatch(getUserFailure(error))
				return false;
			})
	}
}

export const signUp = (user: UserType, type: UserEnumType) => {
	return (dispatch: any) => {
		return signUpNewUser(user, type)
			.then(() => {
				return true;
			})
			.catch(() => {
				return false;
			})
	}
}

export const updateProfile = (user: UserType, type: UserEnumType) => {
	return (dispatch: any) => {
		return updateUser(user, type)
			.then(() => {
				return true;
			})
			.catch(() => {
				return false;
			})
	}
}