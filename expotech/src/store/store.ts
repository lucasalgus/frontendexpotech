import { combineReducers, createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

import { user } from "./user/reducer";
import { event } from "./event/reducer";

const reducers = combineReducers({
	user,
	event
});

export const store = createStore(
	reducers,
	composeWithDevTools(applyMiddleware(thunk))
);