import { fetchAllEvents, postEvent } from "../../api/api";

export const GET_EVENTS_BEGIN = "GET_EVENTS_BEGIN";
export const GET_EVENTS_SUCCESS = "GET_EVENTS_SUCCESS";
export const GET_EVENTS_FAILURE = "GET_EVENTS_FAILURE";

export const getEventsBegin = () => ({
	type: GET_EVENTS_BEGIN,
});

export const getEventsSuccess = (events: any[]) => ({
	type: GET_EVENTS_SUCCESS,
	events
});

export const getEventsFailure = (error: any) => ({
	type: GET_EVENTS_FAILURE,
	error
});

export const getAllEvents = (type: string) => {
	return (dispatch: any) => {
		dispatch(getEventsBegin());

		return fetchAllEvents(type)
			.then(response => {
				dispatch(getEventsSuccess(response));
				return response;
			})
			.catch(error => {
				dispatch(getEventsFailure(error))
				return error;
			})
	}
}

export const createNewEvent = (event: any) => {
	return (dispatch: any) => {
		return postEvent(event)
			.then(response => {
				console.log(response)
				return response;
			})
			.catch(error => {
				console.log(error);
				return error;
			})
	}
}