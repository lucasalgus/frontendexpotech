import { GET_EVENTS_BEGIN, GET_EVENTS_SUCCESS, GET_EVENTS_FAILURE } from "./actions";

const initialState: any = {
	status: null,
	events: [],
	error: null
}

export const event = (state = initialState, action: any) => {
	switch (action.type) {
		case GET_EVENTS_BEGIN:
			return {
				status: "loading"
			}
		case GET_EVENTS_SUCCESS:
			return {
				status: "success",
				events: action.events
			}
		case GET_EVENTS_FAILURE:
			return {
				status: "error",
				error: action.error
			}
		default:
			return state;
	}
}