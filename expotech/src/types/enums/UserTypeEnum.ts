export const UserEnum = {
	ORGANIZER: "ORGANIZER",
	EXHIBITOR: "EXHIBITOR"
};

export type UserEnumType = "ORGANIZER" | "EXHIBITOR";