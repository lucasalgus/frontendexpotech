export type UserType = {
	avatar?: string;
	document?: string;
	email: string;
	id?: number;
	name?: string;
	password: string;
	site?: string;
}