import { UserType } from "../User";
import { UserEnumType } from "../enums/UserTypeEnum";

export type LoginResponseType = {
	auth: boolean;
	user: UserType;
	userType: UserEnumType;
}