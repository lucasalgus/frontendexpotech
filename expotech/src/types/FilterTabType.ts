export type FilterTabType = {
	label: string;
	value: string;
	active: boolean;
}